
from flask import Flask, request, render_template, redirect, url_for, json, session
from flask_mail import Mail, Message

app = Flask(__name__)


app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'


app.config['MAIL_SERVER'] = 'smtp.mail.ru'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = ''  # введите свой адрес электронной почты здесь
app.config['MAIL_DEFAULT_SENDER'] = ''  # и здесь
app.config['MAIL_PASSWORD'] = ''  # введите пароль

mail = Mail(app)

PRICE_LIST={'landing':20000, 'multipage':30000, 
			'store': 40000, 'catalog': 30000, 'forum': 35000, 'agregator': 38000, 'corporat':40000, 'undeterm': 30000,
			'yes': 10000, 'no': 0,
			'templ': 8000, 'individual': 15000, 'clients': 0,
			'translate': 8000, 'robot': 15000, 'mailer': 3000}


FORM_NAMES=['site_type', 'SEO', 'design']


@app.route("/", methods=["GET", "POST"])
def index():
	if request.method == "POST":
		db_user = request.form['name']
		db_tel = request.form['phone']
		db_email = request.form['email']
		msg = Message("Вы оставили форму обратной связи", sender="",  recipients=[db_email])
		msg.body = "You have received a new feedback from {} <{}>.".format(db_user, db_email)
		mail.send(msg)
	return render_template("index.html")


@app.route("/form/<int:i>", methods=["GET", "POST"])
def calc(i):
	if 'price' not in session or i==1:
		session['price'] = 0
	if request.method == "POST":
		try:
			subs = request.form['subsite_type']
		except KeyError:
			subs = None
		if not subs:
			session['price'] += PRICE_LIST[request.form[FORM_NAMES[i-1]]]
		else:
			session['price']+=PRICE_LIST[request.form['subsite_type']]
		return redirect('/form/' + str(i+1))
	return render_template("form" + str(i) + ".html", num=i)


@app.route("/form_last/<int:i>", methods=["GET", "POST"])
def get_price(i):
	if request.method == "POST":
		checked = request.form.getlist('addition')
		for point in checked:
			session['price'] += PRICE_LIST[point]
		output = 'Стоимость разработки от {} рублей'.format(session['price'])
	return render_template("price.html", num = i, price = output)


if __name__ == '__main__':
	app.run(debug=True)