$("input[name=phone]").mask("+7 (999) 999-99-99");


$(window).scroll(function() {
    if ($(this).scrollTop() > 1600) {
        $('.page-up').fadeIn();
    } else {
    $('.page-up').fadeOut();
    }
});


$("a").click( function() {
    const _href = $(this).attr("href");
    $("html, body").animate({scrollTop: $(_href).offset().top+"px"});
    return false;
});

window.addEventListener('DOMContentLoaded', () => {
    const menu = document.querySelector('.menu'),
    menuItem = document.querySelectorAll('.menu_item'),
    hamburger = document.querySelector('.hamburger');

    hamburger.addEventListener('click', () => {
        hamburger.classList.toggle('hamburger_active');
        menu.classList.toggle('menu_active');
    });

    menuItem.forEach(item => {
        item.addEventListener('click', () => {
            hamburger.classList.toggle('hamburger_active');
            menu.classList.toggle('menu_active');
        });
    });

    $("a[href^='#description'], a[href^='#products'], a[href^='#team'], a[href^='#principles'], a[href^='#contacts']").click( function() {
        hamburger.classList.toggle('hamburger_active');
        menu.classList.toggle('menu_active');
    });
});


//Модальные окна
$('[data-modal=order]').on('click', function() {
    $('.overlay, #thanks').fadeOut('0.5s');  
    $('.overlay, #order').fadeIn('0.5s');      
});

$('.modal__close').on('click', function() {
    $('.overlay, #order, #thanks').fadeOut('slow');
});

$('.form').submit(function(e) {
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: "/",
      data: $(this).serialize()
    }).done(function() {
      $(this).find("input").val("");
      $('#order').fadeOut();
      $('.overlay, #thanks').fadeIn('slow');
      $('form').trigger('reset');
    });
    return false;
});

$('#landing').on('click', function() {
    $('.subform').attr('style', 'opacity: 0%;');
    $('.btn_calc').attr('style', 'margin-top: -140px;');
    $('.btn_submit').prop('type','submit');
});

$('#multipage').on('click', function() {
    $('.subform').attr('style', 'opacity: 100%;');
    $('.btn_calc').attr('style', 'margin-top: 25px;');
    $('.btn_submit').prop('type','button');
});

$('.btn_submit').on('click', function() {
    if (this.type == 'button') {
        $('.error').attr('style', 'display: block;');
    } 
});

function Submit(e) {
    $(`input[name=${e}]`).on('click', function() {
        $('.btn_submit').prop('type','submit');
    });
}

const list_input = ['subsite_type', 'SEO', 'design', 'addition'];
list_input.forEach(Submit);


